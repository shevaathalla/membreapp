const data = [
  {
    "name": "Sheva Athalla",
    "description": 
            "Saya adalah seorang mahasiswa semester 7 di Universitas Islam Negeri Malang. Saya berkuliah di jurusan Teknik Informatika. Saya memiliki hobi bermain game dan berolahraga. Saya juga memiliki hobi bermain musik. Saya berharap dapat menyelesaikan tugas ini dengan baik dan tepat waktu.",
    "image": "sheva.jpeg"
  },
  {
    "name": "Mohammad Habib R ",
    "description":
        "Nama saya Mohammad Habib R dan biasa dipanggil Habib. Saya punya banyak hobi, tetapi memasak adalah hobi yang paling saya suka. Ketika ada waktu luang, saya suka memasak. Saya suka memasak semenjak saya kecil. Pada waktu itu, saya sering melihat ayah saya memasak. Seringnya saya memasak sendiri, namun terkadang ayah saya membantu saya. Sebenarnya beliaulah yang mengajarkan saya memasak. Saya sangat suka memasak karena memasak membuat saya senang. Saya merasa bangga setiap melihat seseorang memakan makanan yang saya buat, utamanya ketika ia memuji makanannya. Saya harap suatu hari saya bisa menjadi koki hebat seperti ayah saya.",
    "image": "habib.jpeg"
  },
  {
    "name": "Farhan Rafif Azzufar",
    "description":
        "Saya adalah mahasiswa semester 7 jurusan teknik informatika, saya adalah mahasiswa yang memiliki minat pada bidang animasi dan game development. hal yang membuat saya tertarik pada bidang ini karena saya menyukai cerita yang divisualisasikan dalam gambar baik dalam bentuk 2 dimensi maupun 3 dimensi. selain itu saya juga tertarik dalam bidang game karena saat ini game merupakan platform yang cukup krusial dalam era saat ini, berbagai metode pembelajaran periklanan, serta bisnis pun dapat dijalankan melalui platform ini. karena hal ini saya sangat tertarik dalam bidang animasi dan game development.",
    "image": "farhan.jpeg",
  },
];
