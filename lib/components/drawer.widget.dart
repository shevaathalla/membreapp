import 'package:flutter/material.dart';
import 'package:utsapp/screens/members.screen.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({super.key, required this.username});

  final String username;

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text(username),
              accountEmail: Text(username + '@gmail.com'),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png"),
              ),
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Members'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => MembersScreen()));
              },
            ),
          ],
        ),
      );      
  }
}