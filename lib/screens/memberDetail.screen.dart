import 'package:flutter/material.dart';

class MemberDetailScreen extends StatelessWidget {
  MemberDetailScreen({super.key, required this.member});
  final member;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              snap: false,
              floating: false,
              expandedHeight: 200.0,
              flexibleSpace: FlexibleSpaceBar(background: 
              member["image"] != null ? Image.asset('assets/'+ member["image"]!, fit: BoxFit.cover) : Image.network("https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460__340.png", fit: BoxFit.cover)
              ),              
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: 40,
                child: Align(
                  alignment: AlignmentDirectional.center,
                  child: Text(
                    member["name"]!,
                    style: const TextStyle(
                        fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: 210,
                child: Align(
                  alignment: AlignmentDirectional.topCenter,
                  widthFactor: 1,
                  child: Text(
                    member["description"]!,
                    textAlign: TextAlign.center,
                    style: const TextStyle(                                                
                        fontSize: 16),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
