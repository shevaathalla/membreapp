import 'package:flutter/material.dart';
import 'package:utsapp/components/drawer.widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key, required this.username});

  final String username;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      drawer: DrawerWidget(username: username),
      body: Center(child: Text('Hello ' + username)),
    );
  }
}
