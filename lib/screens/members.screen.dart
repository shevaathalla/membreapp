import 'package:flutter/material.dart';
import 'package:utsapp/data/members.dart' as members;
import 'package:utsapp/screens/memberDetail.screen.dart';

class MembersScreen extends StatelessWidget {
  const MembersScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Member List'),
        ),
        body: MemberList());
  }
}

class MemberList extends StatelessWidget {
  MemberList({super.key});

  @override
  Widget build(BuildContext context) {
    final listOfMembers = members.data;
    return ListView(
      children: [
        for (var member in listOfMembers)
          ListTile(
            leading: CircleAvatar(
              backgroundImage: AssetImage("assets/${member['image']}"),
            ),
            title: Text(member["name"]!),
            subtitle: Text("member"),
            onTap: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MemberDetailScreen(
                            member: member,
                          )))
            },
          )
      ],
    );
  }
}
